Movie Match - Level 4 Project, Matthew Biggins, 2135573b
--------------------------------------------------------

This zip directory contains the source code for the Movie Match Google Assistant Action.
The whole project consists of the following system components:

* A Google Cloud Function containing the Action's webhook code
* A Google Firestore database containing user, group, movie and ratings data
* A Google App Engine instance which exposes a recommendation service via an API

Due to the large amount of component setup, I would advise that the system is not re-built from scratch.

If you wish to test the functionalities of the agent, it is publically deployed on the Actions on Google directory for Google assistant.
The action can be invoked by searching for Movie Match in the Actions directory, or asking your Assistant "Talk to Movie Match."
The action does require users to link their google account for authentication.

Collection and usage of user's data is outlined in the Privacy Policy and Terms of Service:
https://docs.google.com/document/d/1XCAv1I--rrztbdFD3TE7X3M7sQibDzxYjoKjR2OyU34

The agent's source code can be inspected within the file firebase > functions > index.js 
The movie data collected for the Firestore database is stored in data > filtered_movies.json
The recommendation service code can be inspected in surprise-app > app > main.py and recommendations.py

Two python notebooks are also available which show the recommendation model training and evaluation code:
Trainer_Updated_Surprise_Model.ipynb
Evaluation.ipynb

The project's repository is publicly hosted at:
https://gitlab.com/MBigginsUOG/collabrec

