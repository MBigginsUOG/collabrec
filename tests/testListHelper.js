function listHelper(movies) {
  const title = 'Watchlist';
  var builditems = {};

  for (n = 0; n < movies.length; n++){
    var item = {
      synonyms: n+"",
      title: movies[n],
    }
    builditems[[n+""]] = item;
  }
  // return new List({items:builditems});
  return {items:builditems};
}

console.log(listHelper(["Dory","Nemo","Toy Story"]));

function newListBuilder(conv, list_title, list_items){
  console.log('Building new list');
  built_items = {};

  for (n = 0;n < list_items.length;n++){
    let item = {
      title       : list_items[n].title,
      description : `Added by: Matthew  \n`+ list_items[n].plot,
      synonyms    : [String(n),list_items[n].title],
      image       : {
        url : list_items[n].poster,
        alt : `Poster for ${list_items[n].title}`,
      }
    }
    // let key = [list_items[n].title];
    // built_items[key] = item;
    built_items[[n+""]] = item;
  }

  return {title : list_title, items: built_items};
}
//
// console.log(newListBuilder({}, 'Watchlist Title', [{ year: [ 2002 ],
//     imdbVotes: 469154,
//     plot: 'Forced to spend his summer holidays with his muggle relations, Harry Potter gets a real shock when he gets a surprise visitor: Dobby the house-elf, who warns Harry Potter against returning to Hogwarts, for terrible things are going to happen. Harry decides to ignore Dobby\'s warning and continues with his pre-arranged schedule. But at Hogwarts, strange and terrible things are indeed happening: Harry is suddenly hearing mysterious voices from inside the walls, muggle-born students are being attacked, and a message scrawled on the wall in blood puts everyone on his/her guard - "The Chamber Of Secrets Has Been Opened. Enemies Of The Heir, Beware" .',
//     director: [ 'Chris Columbus' ],
//     metascore: 63,
//     rankedRating: 7.379159342079069,
//     genre: [ 'Adventure', 'Family', 'Fantasy', 'Mystery' ],
//     imdbRating: 7.4,
//     actors:
//      [ 'Daniel Radcliffe',
//        'Rupert Grint',
//        'Emma Watson',
//        'Richard Griffiths' ],
//     imdbID: 'tt0295297',
//     ratings: [ ],
//     runtime: '161 min',
//     released:  { _seconds: 1037318400, _nanoseconds: 0 },
//     poster: 'https://m.media-amazon.com/images/M/MV5BMTcxODgwMDkxNV5BMl5BanBnXkFtZTYwMDk2MDg3._V1_SX300.jpg',
//     title: 'Harry Potter and the Chamber of Secrets' },
//   { ratings: [ ],
//     runtime: '194 min',
//     released:  { _seconds: 882489600, _nanoseconds: 0 },
//     poster: 'https://m.media-amazon.com/images/M/MV5BMDdmZGU3NDQtY2E5My00ZTliLWIzOTUtMTY4ZGI1YjdiNjk3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SX300.jpg',
//     title: 'Titanic',
//     year: [ 1997 ],
//     imdbVotes: 913780,
//     plot: '84 years later, a 100 year-old woman named Rose DeWitt Bukater tells the story to her granddaughter Lizzy Calvert, Brock Lovett, Lewis Bodine, Bobby Buell and Anatoly Mikailavich on the Keldysh about her life set in April 10th 1912, on a ship called Titanic when young Rose boards the departing ship with the upper-class passengers and her mother, Ruth DeWitt Bukater, and her fiancé, Caledon Hockley. Meanwhile, a drifter and artist named Jack Dawson and his best friend Fabrizio De Rossi win third-class tickets to the ship in a game. And she explains the whole story from departure until the death of Titanic on its first and last voyage April 15th, 1912 at 2:20 in the morning.',
//     director: [ 'James Cameron' ],
//     metascore: 75,
//     rankedRating: 7.787957852483693,
//     genre: [ 'Drama', 'Romance' ],
//     imdbRating: 7.8,
//     actors:
//      [ 'Leonardo DiCaprio',
//        'Kate Winslet',
//        'Billy Zane',
//        'Kathy Bates' ],
//     imdbID: 'tt0120338' },
//   { actors:
//      [ 'Vera Farmiga',
//        'Patrick Wilson',
//        'Lili Taylor',
//        'Ron Livingston' ],
//     imdbID: 'tt1457767',
//     ratings: [],
//     runtime: '112 min',
//     released:  { _seconds: 1374188400, _nanoseconds: 0 },
//     poster: 'https://m.media-amazon.com/images/M/MV5BMTM3NjA1NDMyMV5BMl5BanBnXkFtZTcwMDQzNDMzOQ@@._V1_SX300.jpg',
//     title: 'The Conjuring',
//     year: [ 2013 ],
//     imdbVotes: 377124,
//     plot: 'In 1971, Carolyn and Roger Perron move their family into a dilapidated Rhode Island farm house and soon strange things start happening around it with escalating nightmarish terror. In desperation, Carolyn contacts the noted paranormal investigators, Ed and Lorraine Warren, to examine the house. What the Warrens discover is a whole area steeped in a satanic haunting that is now targeting the Perron family wherever they go. To stop this evil, the Warrens will have to call upon all their skills and spiritual strength to defeat this spectral menace at its source that threatens to destroy everyone involved.',
//     director: [ 'James Wan' ],
//     metascore: 68,
//     rankedRating: 7.473324494112448,
//     genre: [ 'Horror', 'Mystery', 'Thriller' ],
//     imdbRating: 7.5 } ]));
