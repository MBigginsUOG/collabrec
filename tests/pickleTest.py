import pickle
import io

file_name = '../data/movies.csv'
movie_genres = {}
with io.open(file_name, 'r', encoding='ISO-8859-1') as f:
  for line in f:
    line = line.split(',')
    movie_genres[line[0]] = line[2].split('|')

with io.open('pickle','w') as f:
    pickle.dump(movie_genres,f)
