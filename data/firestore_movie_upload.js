const admin          = require('firebase-admin');
const serviceAccount = require('<<SERVICE_KEY>>');
const realData       = require('filtered_movies.json'); //126,654 movies


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://groupmaker-ef747.firebaseio.com"
});

const db = admin.firestore();

batchUpload(realData);

function getRanking(movie){
  /*(WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C where:
  R = average for the movie (mean) = (Rating)
  v = number of votes for the movie = (votes)
  m = minimum votes required to be listed in the Top 250 (currently 3000)
  C = the mean vote across the whole report (currently 6.9)*/

  let v = movie.imdbVotes;
  let m = 3000;
  let R = movie.imdbRating;
  let C = 4.12;
  // let C = 6.9;
  let ranking = (v/(v + m)) * R + (m/(v + m)) * C;
  return ranking;
}


async function batchUpload(data){
  // 126654 movies / 500 writes = 253.3 (254) batches
  var numBatches = Math.ceil(data.length/500);
  var batchCount = 0;

  console.log(`Starting Upload of ${data.length} movies, in ${numBatches} batches.`);
  for (i = 0; i < numBatches; i++){

    var batch = db.batch();
    // If we are on last batch
    if (i === (numBatches-1)){
      // Final chunk is < 500 movies
      var splitData = data.slice(i*500,data.length);
    } else {
      // Split movies into chunks of 500
      var splitData = data.slice(i*500,(i+1) * 500)
    }

    // For each movie in subsection of movies
    for (j = 0; j < splitData.length;j++){
      var obj = splitData[j];
      var newMovieRef = db.collection('movies').doc(obj.imdbID);
      batch.set(newMovieRef,
        {
            actors       : obj.actors,
            poster       : obj.poster,
            released     :(obj.released === "N/A" ? null : new Date(obj.released)),
            year         : obj.year,
            imdbId       : obj.imdbID,
            imdbRating   : obj.imdbRating,
            imdbVotes    : obj.imdbVotes,
            metascore    : obj.metascore,
            ratings      : obj.ratings,
            rankedRating : getRanking(obj),
            runtime      : obj.runtime,
            genre        : obj.genre,
            title        : obj.title,
            plot         : obj.plot,
            director     : obj.director
        }
      );
    }

    // Commit batch of doc writes
    try {
      await batch.commit();
      batchCount++;
      console.log(`Committed batch ${batchCount}.`);
    } catch (e) {
      console.log(e);
    }


  }
}
