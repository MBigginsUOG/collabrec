
from collections import defaultdict
from datetime import datetime
import time
import numpy as np
import pandas as pd
import logging
import os
import io

import google.auth
from google.cloud import storage

logging.basicConfig(level=logging.INFO)

rid_to_imdbid = {}
imdbid_to_rid = {}
with io.open('links.csv', 'r', encoding='ISO-8859-1') as f:
  for line in f:
    line = line.split(',')
    rid_to_imdbid[line[0]] = line[1]
    imdbid_to_rid[line[1]] = line[0]

orig_ratings =  pd.read_csv('ratings.csv', sep=',', header=0)
next_user = orig_ratings['userId'].max() + 1
next_item = orig_ratings['movieId'].max() + 1

# // user id is split by (',')[1].split('""')[1]
# // iid is split by (,) []
# // rating is split by (,) [5]
# // time is split by (,) [6]

new_user_map = {}
new_rating_lines = []
new_item_map = {}
with io.open('exported_ratings.csv', 'r', encoding='ISO-8859-1') as f:
  for line in f:
    if (line != 'userId,imdbId,rating,timestamp\n'):
    # if (line != '__key___path,__key___name,rating,timestamp\n'):
        # Split line and remove newline
        line = line.split(',')
        line[3] = line[3][:-1]

        # Remove 'tt' from imdbId
        line[1] = line[1][2:]

        # Check user map for user
        if (line[0] not in new_user_map.keys()):
            # Add new key if unkown userId
            new_user_map[line[0]] = str(next_user)
            next_user += 1

        # Check item map for imdbid
        if (line[1] not in imdbid_to_rid.keys()):
            # Add new key if unknown id
            imdbid_to_rid[line[1]] = str(next_item)
            rid_to_imdbid[next_item] = line[1]
            new_item_map[str(next_item)] = line[1]
            next_item += 1

        userId = new_user_map[line[0]]
        itemId = imdbid_to_rid[line[1]]
        rating = float(line[2])
        timestamp = int(time.mktime(time.strptime(line[3], '%Y-%m-%d %H:%M:%S.%f %Z')))

        new_rating_lines.append([userId,itemId,rating,timestamp])

# Create dict of values
d = {
     'userId'    : [x[0] for x in new_rating_lines],
     'movieId'   : [x[1] for x in new_rating_lines],
     'rating'    : [x[2] for x in new_rating_lines],
     'timestamp' : [x[3] for x in new_rating_lines]
     }

# Create new pandas df
new_ratings_df = pd.DataFrame(data=d,columns=['userId','movieId','rating','timestamp'])
new_ratings_df = new_ratings_df.sort_values(['movieId'])
new_ratings_df = new_ratings_df.groupby(['userId','movieId']).max()

# Output to csv ( then read back in because i dont know how pandas works)
new_ratings_df.to_csv('new_ratings.csv', header=True, mode = 'w')
new_ratings_df = pd.read_csv('new_ratings.csv', header=0)

# Write user map for firestore users to raw user ids
with io.open('user_links.csv', 'w', encoding='ISO-8859-1') as f:
    for k in new_user_map.keys():
        f.write(k + ',' + new_user_map[k] + '\n')

# Append new item ids to links.csv
with io.open('links.csv', 'a', encoding='ISO-8859-1') as f:
    for k in new_item_map.keys():
        f.write(k + ',' + new_item_map[k] + ',' + '0' + '\n')

final_ratings_df = orig_ratings.append(new_ratings_df)
final_ratings_df.to_csv('train_ratings.csv',header=True,mode='w',index=False)


# Upload files to GCS
def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}.'.format(
        source_file_name,
        destination_blob_name))

upload_blob('recserve_groupmaker-ef747','train_ratings.csv','data/train_ratings.csv')
upload_blob('recserve_groupmaker-ef747','links.csv','data/links.csv')
upload_blob('recserve_groupmaker-ef747','user_links.csv','data/user_links.csv')
