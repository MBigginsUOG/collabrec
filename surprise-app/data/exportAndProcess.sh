#!/bin/bash

# Export from Firestore to GCS
export TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
gcloud beta firestore export gs://gm-ratings-export/$TIMESTAMP --collection-ids=user_ratings
wait
# Load from GCS to bigquery
bq load --replace --source_format=DATASTORE_BACKUP gm_ratings_export.latest gs://gm-ratings-export/$TIMESTAMP/all_namespaces/kind_user_ratings/all_namespaces_kind_user_ratings.export_metadata
wait
# Query to select rows from exported data
bq query --destination_table gm_ratings_export.latestQuery --replace --use_legacy_sql=false 'SELECT userId,imdbId,rating,timestamp FROM `groupmaker-ef747.gm_ratings_export.latest` ORDER BY userId ASC,timestamp ASC'
wait

# Export back to GCS (the only option for exporting queries from BigQuery as CSV!)
bq extract --compression GZIP 'gm_ratings_export.latestQuery' gs://gm-ratings-export/exported_ratings.gz
wait

# Download from GCS to local
gsutil cp gs://gm-ratings-export/exported_ratings.gz .
wait

# Unzip .gzip file
gzip -d exported_ratings.gz
cp exported_ratings exported_ratings.csv
rm exported_ratings
wait

# Process - python3 process_new_ratings.py
python3 process_new_ratings.py
wait

# Upload new links.csv, user_links.csv to gcs
gsutil cp links.csv gs://recserve_groupmaker-ef747/data/links.csv
gsutil cp user_links.csv gs://recserve_groupmaker-ef747/data/user_links.csv
wait

# Retrain and upload new model.
echo "To retrain the model with new ratings, you must use the Colab model trainer notebook: https://colab.research.google.com/drive/1DAO3zj3mvbVevnMbKQ-pvPEr6SB9wqPa"

# Re-version or restart app engine
echo "Once the model is retrained, please restart the recserve App Engine instance: https://console.cloud.google.com/appengine/versions?project=groupmaker-ef747&serviceId=default&versionssize=50"
