from collections import defaultdict
from surprise import accuracy
from surprise import dump
from surprise import Dataset
from surprise import get_dataset_dir
from surprise import Reader
from surprise import SVD
from surprise.model_selection import train_test_split
import io
import os
from datetime import date

# path to dataset file
file_path = ('/home/matthew/uni/level4/project/collabrec/surprise-app/data/train_ratings.csv')

# As we're loading a custom dataset, we need to define a reader. In the
# movielens-100k dataset, each line has the following format:
# 'user item rating timestamp', separated by '\t' characters.
reader = Reader(line_format='user item rating timestamp', sep=',',skip_lines=1)

data = Dataset.load_from_file(file_path, reader=reader)

# sample random trainset and testset
# test set is made of 25% of the ratings.
# trainset, testset = train_test_split(data, test_size=.25)

# Build full trainset as we are training for production
trainset = data.build_full_trainset()
testset = trainset.build_anti_testset()

# We'll use the famous SVD algorithm.
algo = SVD()

# Train the algorithm on the trainset, and predict ratings for the testset
algo.fit(trainset)
print('Finished fitting.')
predictions = algo.test(testset)
print('Predictions made.')

# Then compute RMSE
# accuracy.rmse(predictions)

today = str(date.today())

file_name = os.path.expanduser('../model/model-'+today)
print('Dumping model.')
dump.dump(file_name, algo=algo, predictions=predictions)
print('Complete.')
