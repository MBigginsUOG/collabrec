# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Recommendation generation module."""

import logging
import numpy as np
import os
import pandas as pd
from collections import defaultdict
from surprise import accuracy
from surprise import dump
from surprise import Dataset
from surprise import get_dataset_dir
from surprise import Reader
from surprise import SVD
from surprise.model_selection import train_test_split
import io
import os
from datetime import date

import google.auth
import google.cloud.storage as storage

logging.basicConfig(level=logging.INFO)

LOCAL_MODEL_PATH = '/tmp'

MODEL_FILE = 'model/model'
RATINGS_FILE = 'data/ratings.csv'
ITEM_LINKS_FILE = 'data/links.csv'
MOVIES_FILE = 'data/movies.csv'


class Recommendations(object):
  """Provide recommendations from a pre-trained collaborative filtering model.

  Args:
    local_model_path: (string) local path to model files
  """

  def __init__(self, local_model_path=LOCAL_MODEL_PATH):
    _, project_id = google.auth.default()
    self._bucket = 'recserve_' + project_id
    self._load_model(local_model_path)

  def _load_model(self, local_model_path):
    """Load recommendation model files from GCS.

    Args:
      local_model_path: (string) local path to model files
    """
    # download files from GCS to local storage
    if (not os.path.exists('/tmp/model/model') or not os.path.exists('/tmp/data')):
        os.makedirs(os.path.join(local_model_path, 'model'), exist_ok=True)
        os.makedirs(os.path.join(local_model_path, 'data'), exist_ok=True)
        client = storage.Client()
        bucket = client.get_bucket(self._bucket)

        logging.info('Downloading blobs.')

        model_files = [MODEL_FILE,RATINGS_FILE, ITEM_LINKS_FILE, MOVIES_FILE]
        for model_file in model_files:
          blob = bucket.blob(model_file)
          with open(os.path.join(local_model_path, model_file), 'wb') as file_obj:
            blob.download_to_file(file_obj)

        logging.info('Finished downloading blobs.')

    self.predictions,self.algo = dump.load(os.path.join(local_model_path, MODEL_FILE))

    ratings_df = pd.read_csv(os.path.join(local_model_path,
                                        RATINGS_FILE), sep=',', header=0)
    self.user_items = ratings_df.groupby('userId')


    file_name = os.path.join(local_model_path, ITEM_LINKS_FILE)
    rid_to_imdbid = {}
    with io.open(file_name, 'r', encoding='ISO-8859-1') as f:
      for line in f:
        line = line.split(',')
        rid_to_imdbid[line[0]] = line[1]

    self.item_links = rid_to_imdbid

    logging.info('Finished loading model.')

  def get_recommendations(self, user_id, num_recs):
    """Given a user id, return list of num_recs recommended item ids.

    Args:
      user_id: (string) The user id
      num_recs: (int) The number of recommended items to return

    Returns:
      [item_id_0, item_id_1, ... item_id_k-1]: The list of k recommended items,
        if user id is found.
      None: The user id was not found.
    """
    movie_recommendations = None

    top_n = get_top_n(self.predictions, n=num_recs)

    movie_recommendations = top_n_for_user(str(user_id),top_n)[1]

    imdb_ids = [self.item_links[str(id)] for id in movie_recommendations]

    return imdb_ids

    # # map user id into ratings matrix user index
    # user_idx = np.searchsorted(self.user_map, user_id)
    #
    # if user_idx:
    #   # get already viewed items from views dataframe
    #   already_rated = self.user_items.get_group(user_id).itemId
    #   already_rated_idx = [np.searchsorted(self.item_map, i)
    #                        for i in already_rated]
    #
    #   # generate list of recommended article indexes from model
    #   recommendations = generate_recommendations(user_idx, already_rated_idx,
    #                                              self.user_factor,
    #                                              self.item_factor,
    #                                              num_recs)
    #
    #   # map article indexes back to article ids
    #   article_recommendations = [self.item_map[i] for i in recommendations]

def get_top_n(predictions, n=10):
    '''Return the top-N recommendation for each user from a set of predictions.

    Args:
        predictions(list of Prediction objects): The list of predictions, as
            returned by the test method of an algorithm.
        n(int): The number of recommendation to output for each user. Default
            is 10.

    Returns:
    A dict where keys are user (raw) ids and values are lists of tuples:
        [(raw item id, rating estimation), ...] of size n.
    '''

    # First map the predictions to each user.
    top_n = defaultdict(list)
    for uid, iid, true_r, est, _ in predictions:
        top_n[uid].append((iid, est))

    # Then sort the predictions for each user and retrieve the k highest ones.
    for uid, user_ratings in top_n.items():
        user_ratings.sort(key=lambda x: x[1], reverse=True)
        top_n[uid] = user_ratings[:n]

    return top_n

def top_n_for_user(target_uid,top_n):
    for uid, user_ratings in top_n.items():
      if uid == target_uid:
        return (uid, [iid for (iid, _) in user_ratings])

# def read_item_names():
#     """Read the u.item file from MovieLens 100-k dataset and return two
#     mappings to convert raw ids into movie names and movie names into raw ids.
#     """
#
#     file_name = get_dataset_dir() + '/ml-100k/ml-100k/u.item'
#     rid_to_name = {}
#     name_to_rid = {}
#     with io.open(file_name, 'r', encoding='ISO-8859-1') as f:
#         for line in f:
#             line = line.split('|')
#             rid_to_name[line[0]] = line[1]
#             name_to_rid[line[1]] = line[0]
#
#     return rid_to_name, name_to_rid

rec_util = Recommendations()

rec_list = rec_util.get_recommendations(str(6), 5)
print(rec_list)
