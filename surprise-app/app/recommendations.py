# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Recommendation generation module."""

import logging
import numpy as np
import os
import pandas as pd
from collections import defaultdict
from surprise import accuracy
from surprise import dump
from surprise import Dataset
from surprise import get_dataset_dir
from surprise import Reader
from surprise import SVD
from surprise.model_selection import train_test_split
import io
import os
from datetime import date

import google.auth
import google.cloud.storage as storage

logging.basicConfig(level=logging.INFO)

LOCAL_MODEL_PATH = '/tmp'

MODEL_FILE = 'model/model'
ITEM_LINKS_FILE = 'data/links.csv'
USER_LINKS_FILE = 'data/user_links.csv'


class Recommendations(object):
  """Provide recommendations from a pre-trained collaborative filtering model.

  Args:
    local_model_path: (string) local path to model files
  """

  def __init__(self, local_model_path=LOCAL_MODEL_PATH):
    _, project_id = google.auth.default()
    self._bucket = 'recserve_' + project_id
    self._load_model(local_model_path)

  def _load_model(self, local_model_path):
    """Load recommendation model files from GCS.

    Args:
      local_model_path: (string) local path to model files
    """
    # download files from GCS to local storage
    if (not os.path.exists('/tmp/model/model') or not os.path.exists('/tmp/data')):
        os.makedirs(os.path.join(local_model_path, 'model'), exist_ok=True)
        os.makedirs(os.path.join(local_model_path, 'data'), exist_ok=True)
        client = storage.Client()
        bucket = client.get_bucket(self._bucket)

        logging.info('Downloading blobs.')

        model_files = [MODEL_FILE, ITEM_LINKS_FILE, USER_LINKS_FILE]
        for model_file in model_files:
          blob = bucket.blob(model_file)
          with open(os.path.join(local_model_path, model_file), 'wb') as file_obj:
            blob.download_to_file(file_obj)

        logging.info('Finished downloading blobs.')

    self.predictions,self.algo = dump.load(os.path.join(local_model_path, MODEL_FILE))


    file_name = os.path.join(local_model_path, ITEM_LINKS_FILE)
    rid_to_imdbid = {}
    with io.open(file_name, 'r', encoding='ISO-8859-1') as f:
      for line in f:
        line = line.split(',')
        rid_to_imdbid[line[0]] = line[1]

    file_name = os.path.join(local_model_path, USER_LINKS_FILE)
    fsid_to_uid = {}
    with io.open(file_name, 'r', encoding='ISO-8859-1') as f:
      for line in f:
        line = line.split(',')
        fsid_to_uid[line[0]] = line[1][:-1]

    self.item_links = rid_to_imdbid
    self.user_links = fsid_to_uid

    logging.info('Finished loading model.')

  def get_recommendations(self, user_ids, num_recs):
    """Given a list of user ids, return list of num_recs recommended item ids.

    Args:
      user_id: (string) The user id
      num_recs: (int) The number of recommended items to return

    Returns:
      [item_id_0, item_id_1, ... item_id_k-1]: The list of k recommended items,
        if user id is found.
      None: The user id was not found.
    """
    movie_recommendations = None
    logging.info('TYPE:',type(user_ids),'IDS:',str(user_ids))

    raw_user_ids = [self.user_links.get(u) for u in user_ids]
    raw_user_ids = [u for u in raw_user_ids if u is not None]
    if (not raw_user_ids):
        logging.info('User id not in user_links:', str(user_ids))
        return None

    top_n = get_top_n(self.predictions, raw_user_ids, n=100)
    group_top_n = self.top_n_for_group(raw_user_ids,top_n,num_recs)

    if (not group_top_n):
        logging.info('No top recs found for these users.', str(user_ids))
        return None

    imdb_ids = ['tt' + self.item_links[str(id)] for id in group_top_n]

    return imdb_ids

  # Aggregates the top 10 recommended items for each user with multiplicative strategy
  def top_n_for_group(self,target_users,top_n,n):
    group_top_ns = list()
    for uid, user_ratings in top_n.items():
      if uid in target_users:
        for (iid, _) in user_ratings:
          group_top_ns.append(iid)

    preds = dict()
    for iid in group_top_ns:
      group_rating = 1
      for uid in target_users:
        pred = self.algo.predict(uid, iid,verbose=False)
        group_rating *= pred.est
      preds[iid] = group_rating

    sorted_by_value = sorted(preds.items(), key=lambda kv: kv[1],reverse=True)
    return [iid for (iid,_) in sorted_by_value][:n]

def get_top_n(predictions, target_users, n=10):
    '''Return the top-N recommendation for each user from a set of predictions.

    Args:
        predictions(list of Prediction objects): The list of predictions, as
            returned by the test method of an algorithm.
        n(int): The number of recommendation to output for each user. Default
            is 10.

    Returns:
    A dict where keys are user (raw) ids and values are lists of tuples:
        [(raw item id, rating estimation), ...] of size n.
    '''

    # First map the predictions to each user.
    top_n = defaultdict(list)
    for uid, iid, true_r, est, _ in predictions:
        if uid in target_users:
            top_n[uid].append((iid, est))

    # Then sort the predictions for each user and retrieve the k highest ones.
    for uid, user_ratings in top_n.items():
        user_ratings.sort(key=lambda x: x[1], reverse=True)
        top_n[uid] = user_ratings[:n]

    return top_n
