const {dialogflow, SimpleResponse, BasicCard, Button, Image, Carousel, List, Confirmation, SignIn, Suggestions} = require('actions-on-google');
const admin       = require('firebase-admin');
const functions   = require('firebase-functions');
const axios       = require('axios');
const randomWords = require('random-words');
const settings    = {/* your settings... */ timestampsInSnapshots: true};
// const chatbase    = require('@google/chatbase')
//                     .setApiKey(process.env.chatbase_key) // Your Chatbase API Key
//                     .setPlatform('Google Assistant - Movie Match') // The platform you are interacting with the user over
//                     .setAsTypeUser(); // The type of message you are sending to chatbase: user (user) or agent (bot)

admin.initializeApp(functions.config().firebase);


// Module setups and API keys
const app = dialogflow({
  clientId: '90566934360-o29tdbgg4sg681mf7ctkhm52rkji9l9f.apps.googleusercontent.com',
});

const auth = admin.auth();
const db = admin.firestore();
const chatbase_key = '2d0c48ae-45d5-4d30-b364-8ca9f9e72710';
const baseUrl = 'https://api.themoviedb.org/3/', baseImgUrl = 'https://image.tmdb.org/t/p/w342';
const tmdbApiKey = 'ec080b067cda6ab8ab01a853231a54a4';
const omdbApiKey = 'af81d853';
const omdbApiBase = `http://www.omdbapi.com/?apikey=${omdbApiKey}&`;

db.settings(settings);

// TMDb Genre codes
const tmdbGenres = {
  ['Action']      : 28,
  ['Adventure']   : 12,
  ['Animation']   : 16,
  ['Comedy']      : 35,
  ['Crime']       : 80,
  ['Documentary'] : 99,
  ['Drama']       : 18,
  ['Family']      : 10751,
  ['Fantasy']     : 14,
  ['History']     : 36,
  ['Horror']      : 27,
  ['Musical']     : 10402,
  ['Mystery']     : 9648,
  ['Romance']     : 10749,
  ['Sci-Fi']      : 878,
  ['Thriller']    : 53,
  ['War']         : 10752,
  ['Western']     : 37,
}

// Suggestion chip lists! use SUGGS['name'] to get a particular list of chips for your response
const SUGGS = {
  ['default']         : ['Top Recommendations 🎥','Movie Match 🕵🏽','Show our watch list 🍿','Group Summary 📑','List Groups 📜','Quick Ratings ⚡','Help ❓'],
  ['welcome']         : ['Top Recommendations 🎥','Movie Match 🕵🏽','Show our watch list 🍿','Group Summary 📑','List Groups 📜','Help ❓'],
  ['ratings']         : ['🔙 Back','Add to Watch list 📜','Similar Movies 🎥','1⭐','2⭐⭐','3⭐⭐⭐','4⭐⭐⭐⭐','5⭐⭐⭐⭐⭐'],
  ['in_wlist']        : ['🔙 Back','Remove from Watch list 🗑️','Similar Movies 🎥','1⭐','2⭐⭐','3⭐⭐⭐','4⭐⭐⭐⭐','5⭐⭐⭐⭐⭐'],
  ['quick ratings']   : ['🤷 Skip','1⭐','2⭐⭐','3⭐⭐⭐','4⭐⭐⭐⭐','5⭐⭐⭐⭐⭐','Add to Watchlist 📜','Quit ❌'],
  ['recommendations'] : ['More ♻️','Go home 🏠','Show our watch list 🍿','Group Summary 📑','List Groups 📜','Help ❓'],
  ['similar']         : ['🔙 Back','Go home 🏠','Show our watch list 🍿','Group Summary 📑','List Groups 📜','Help ❓'],
  ['search']          : ['Something scary 😱','Something funny 😂','An action movie 🤯!','Definitely Sci-Fi 🚀','Animation ✏️'],
  ['biasing']         : ['Scarier!😱','Not as scary...😱','Funnier!😂','Less violent...🔞','More Sci-Fi🚀','More cartoony','More Action🤯'],
  ['yes or no']       : ['Yes 👍','No 👎'],
  ['create group']    : 'Create a Group 👨‍👩‍👧‍👦',
  ['rename group']    : 'Rename Group 📝',
  ['list groups']     : 'List Groups 📜',
  ['invite to group'] : 'Invite to Group 📨',
  ['group summary']   : 'Group Summary 📑',
  ['show movies']     : 'Show me some movies 🎥',
  ['watchlist']       : 'Show our watch list 🍿',
  ['help']            : 'Help ❓',
  ['yes']             : 'Yes 👍',
  ['no']              : 'No 👎',
  ['cancel']          : 'Cancel ❌',
  ['more']            : 'More ♻️',
  ['back']            : 'Back 🔙',
  ['1star']           : '⭐',
  ['2stars']          : '⭐⭐',
  ['3stars']          : '⭐⭐⭐',
  ['4stars']          : '⭐⭐⭐⭐',
  ['5stars']          : '⭐⭐⭐⭐⭐',
}

/*
* Helper Functions
* ---------------
* These functions abstract some functionality so that they can be repeated in multiple inents.
*/

// RATINGS

/** Gets a list of all ratings from the current group.*/
async function getAllGroupRated(conv){
  console.log('getAllGroupRated helper triggered.');

  let groupDoc = await db.doc('groups/'+conv.user.storage.curr_group_id).get();
  let users    = groupDoc.get('users');

  var userProms = [];
  console.log('users',users);
  for (let u in users){
    console.log('u',u);
    userProms.push(users[u].get());
  }
  let userDocs = await Promise.all(userProms);

  var rated = [];
  for (let doc in userDocs){
    rated = rated.concat(userDocs[doc].get('rated'));
  }

  console.log('Group has rated: ',rated);
  return rated;
}

/** Submits a rating to firestore in all the necessary Firestore locations.*/
async function submitRating(conv,id,rating,title){
  console.log(`Submitting rating by ${conv.user.storage.user_id}:`,id,title,rating);

  try{
    var promises = [];
    promises.push(db.doc('users/' + conv.user.storage.user_id).update({
      rated : admin.firestore.FieldValue.arrayUnion(id)
    }));

    promises.push(db.doc('users/' + conv.user.storage.user_id + '/user_ratings/' + id).set({
      title     : title,
      rating    : rating,
      imdbId    : id,
      userId    : conv.user.storage.user_id,
      timestamp : admin.firestore.Timestamp.now()
    }));

    promises.push(db.collection('ratings').add({
      imdbId    : id,
      rating    : rating,
      in_group  : conv.user.storage.curr_group_id,
      timestamp : admin.firestore.Timestamp.now(),
      userId    : conv.user.storage.user_id,
    }));

    if (conv.user.storage.rated){
      conv.user.storage.rated.push(id);
    } else {
      conv.user.storage.rated = [id];
    }

    return Promise.all(promises);
  } catch(e){
    console.error('Rating failed: ',e);
    return false;
  }
}

/** Converts an integer n to n star emojis */
function starsToEmoji(n){
  return '⭐'.repeat(n);
}

// HELPERS

/** Generates a random 3 word password for group invitations. */
async function passwordGenerator(){

  var password = randomWords({ exactly: 3, join: ' ' });
  var groupSnapshot = await db.collection('groups').where('passphrase','==',password).get();
  if (!groupSnapshot.empty){
    password = passwordGenerator();
  }

  return password;
}

/** Fetches movie data from TMDb for imdb ids that are missing from the firestore database. */
async function fixMissingImdb(id){

  let res = await axios.get(`https://www.omdbapi.com/?apikey=${omdbApiKey}&i=${id}&plot=full`);
  if (res.data.Response === 'False'){
    return console.log(`Imdb Id ${id} not available on OMDb.`);
  }
  var obj = res.data;
  parsed_release_date = (obj.Released === "N/A" ? null : new Date(obj.Released));
  parsed_genres = obj.Genre.split(", ");

  let set = await db.collection('movies').doc(id).set(
    {
      poster       : (obj.Poster === "N/A" ? "https://i.imgur.com/qrMim4w.png" : obj.Poster),
      released     : parsed_release_date,
      year         : [obj.Year],
      imdbId       : obj.imdbID,
      imdbRating   : (obj.imdbRating === "N/A" ? 0 : obj.imdbRating),
      imdbVotes    : (obj.imdbVotes === "N/A" ? 0 : obj.imdbVotes),
      metascore    : (obj.Metascore === "N/A" ? 0 : obj.Metascore),
      rankedRating : topRatedRanking(obj),
      runtime      : obj.Runtime,
      genre        : parsed_genres,
      title        : obj.Title,
      plot         : (obj.Plot === "N/A" ? "Mystery movie! Watch it and find out!" : obj.Plot),
      director     : obj.Director
    }
  );

  let get = await db.collection('movies').doc(id).get();
  console.log(`${id} added to database.`);
  return get.data();
}

/** Selects a random name when a new group is created. */
function selectName() {
  console.log('Selecting random name...');
  const preNames = ['Netflix','Movie','Film','Cinema','DVD','Bluray','Streaming'];
  const postNames = ['Queens','Kings','Friends','Lads','Gals','Babes','Squad'];
  var randPre = preNames[Math.floor(Math.random() * preNames.length)];
  var randPost = postNames[Math.floor(Math.random() * postNames.length)];
  return randPre + " " + randPost;
}

/** Gets the users username if they are signed in. */
function getUserName(conv) {
  const {payload} = conv.user.profile;
  return payload.given_name;
}

/** Resets the conv.user.storage contents*/
function resetUserStorage(conv) {
  const name = conv.user.storage.curr_group_name;
  const id   = conv.user.storage.curr_group_id;

  conv.user.storage                 = {};
  conv.user.storage.curr_group_name = '';
  conv.user.storage.curr_group_id   = '';
}

/** Returns the name of all active contexts.*/
function getActiveContexts(conv){
  var active_contexts = [];
  for (const context of conv.contexts){
    active_contexts.push(context.name);
  }
  return active_contexts;
}

/** Calculates a weighted overall ranking for a new movie based on the IMDb top 250 ranking algorithm. */
function topRatedRanking(movie){
  /*(WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C where:
  R = average for the movie (mean) = (Rating)
  v = number of votes for the movie = (votes)
  m = minimum votes required to be listed in the Top 250 (currently 3000)
  C = the mean vote across the whole report (currently 6.9)*/
  let ranking = 0;
  let v       = parseInt(movie.imdbVotes);
  let m       = 3000;
  let R       = parseInt(movie.imdbRating);
  let C       = 6.9;
  ranking     = (v/(v + m) * R + (m/(v + m)) * C);
  console.log(`${movie.Title} ranking in top 250 is ${ranking}`);
  return ranking;
}

// RESPONSES

/** Builds a card response for a movie using its imdb id.*/
async function buildMovieCard(conv,imdb_id){
  console.log('buildMovieCard helper triggered');

  var movie;
  let doc = await db.collection('movies').doc(imdb_id).get();
  if (doc.exists){
    movie = doc.data()
  } else {
    console.log(imdb_id + ' does not exist.');
    movie = await fixMissingImdb(imdb_id);
  }

  let user_rating = '';
  // let ratingsSnap = await db.collection('users/'+conv.user.storage.user_id+'/ratings').where('imdbId','==',imdb_id).get();
  let ratingsDoc = await db.doc('users/'+conv.user.storage.user_id+'/user_ratings/' + imdb_id).get();
  if (ratingsDoc.exists){
    user_rating = `Your Rating: ${starsToEmoji(ratingsDoc.get('rating'))} | `;
  }

  conv.contexts.set('movie_card_details',2,{title:movie.title,imdbId:imdb_id});

  let card_obj = {
    text: movie.plot,
    title: movie.title + ` (${movie.year[0]})`,
    subtitle: user_rating +`Released: ${movie.released.toDate().toLocaleDateString()} | Director: ${movie.director} | Genre(s): ${movie.genre.join(', ')} | IMDb Rating: ${movie.imdbRating}`,
    image: new Image({
      url: movie.poster,
      alt: `Poster for ${movie.title}`,
    }),
    display: 'CROPPED',
    buttons: new Button({
      title: 'View on IMDb',
      url: `https://www.imdb.com/title/${movie.imdbId}/`,
    }),
  }

  conv.data.response_stack.push({type:'card',title:movie.title,imdbId:imdb_id,contents:card_obj});
  return new BasicCard(card_obj);
  // return new BasicCard({
  //   text: movie.plot,
  //   title: movie.title + ` (${movie.year[0]})`,
  //   subtitle: user_rating +`Released: ${movie.released.toDate().toLocaleDateString()} | Director: ${movie.director} | Genre(s): ${movie.genre.join(', ')} | IMDb Rating: ${movie.imdbRating}`,
  //   image: new Image({
  //     url: movie.poster,
  //     alt: `Poster for ${movie.title}`,
  //   }),
  //   display: 'CROPPED',
  //   buttons: new Button({
  //     title: 'View on IMDb',
  //     url: `https://www.imdb.com/title/${movie.imdbId}/`,
  //   }),
  // });
}

/** Fetches the group's watch list.*/
async function watchlistHelper(conv){
  console.log('New Watch List Helper triggered');

  var groupDoc      = await db.doc('groups/'+conv.user.storage.curr_group_id).get();
  var storedlist    = groupDoc.data().watchlist;
  console.log(storedlist);
  var movieDocs     = [];
  var moviePromises = [];

  if (typeof(storedlist) === 'undefined' || storedlist.length === 0){
    return defaultAsk(conv,'Your group doesn\'t have anything in its watch list yet! Try adding something yourself.');
  } else {
    if (storedlist.length === 1){
      //TODO add Basic card of movie
      let card = await buildMovieCard(conv,storedlist[0]);
      conv.contexts.set('viewing_watchlist',1,{movie_to_remove:storedlist[0],title:card.title});
      conv.ask(`You only have one movie in your watch list. This should be an easy decision!`);
      conv.ask(new Suggestions(SUGGS['in_wlist']));
      return conv.ask(card);
    } else {
      console.log('Pushing promises');
      const results = [];
      for (let id of storedlist){
        moviePromises.push(db.collection('movies').doc(id).get());
      }
      console.log('resolving promises');
      let movieSnaps = await Promise.all(moviePromises);

      // for (let snap of querySnaps){
      //   // console.log(snap);
      //   if (!snap.empty){
      //     movieDocs.push(snap.docs[0].data());
      //   }
      // }

      for (let doc of movieSnaps){
        // console.log(snap);
        if (doc.exists){
          movieDocs.push(doc.data());
        } else {
          /* eslint-disable no-await-in-loop */
          let new_doc = await fixMissingImdb(doc.id);
          movieDocs.push(new_doc);
        }
      }

      // console.log(movieDocs);

      const listObject = movieListBuilder(conv, `${conv.user.storage.curr_group_name} Watchlist 🍿`,movieDocs);
      conv.ask(new Suggestions(SUGGS['default']));
      conv.contexts.set('movie_list_details',3,{title:`${conv.user.storage.curr_group_name} Watchlist 🍿`,items:listObject.items});
      conv.contexts.set('viewing_watchlist',1,{});
      return conv.ask(listObject);
    }
  }
}

/** Gets recommendations from the app engine recommendation service. */
async function getRecs(conv,user_ids,num_recs){
  console.log('Getting movie recommendations.');

  let users_string = user_ids.join(',')
  console.log(users_string);
  var list = [];
  try {
    // let app_response = await axios.get(`https://groupmaker-ef747.appspot.com/recommendation?userIds=bJRITq8qKMZEAr9jBMuSIuV5cBG2&numRecs=${num_recs}`);
    let app_response = await axios.get(`https://groupmaker-ef747.appspot.com/recommendation?userIds=${users_string}&numRecs=${num_recs}`);
    list = app_response.data['imdb ids'];
  } catch (error) {
    console.error(error);
  }

  console.log(list);
  return list;
}

/** Wraps a default "What now?" response*/
function whatNowWrapper(conv){
  let responses = [
    'What would you like to do now?',
    'What would you like to do next?',
    'How would you like to proceed?'
  ]

  conv.ask(new Suggestions(SUGGS['default']));
  conv.ask(responses[Math.floor(Math.random() * responses.length)]);
  return;
}

/** Wraps any response with the default suggestion chips (SUGGS['default'])*/
function defaultAsk(conv,response){
  conv.ask(new Suggestions(SUGGS['default']));
  return conv.ask(response);
}

/** Builds a List response using structured JSON items*/
function listBuilder(listTitle, listItems) {
  var builditems = {};

  for (n = 0; n < listItems.length; n++){
    var item = {
      synonyms: String(n),
      title: listItems[n],
      description: 'Description'
    }
    // builditems[[String(n)]] = item;
    builditems[listItems[n]] = item;
  }
  return new List({title:listTitle ,items:builditems});
}

/** Wraps a response and keeps contexts alive. */
function convWrapper(conv,response){
  conv.data.last_response = response;
  const contexts = getActiveContexts(conv);
  for (const context of contexts){
    conv.contexts.set(context.name,context.lifespan+1,context.parameters);
  }
  return conv.ask(response);
}

/** Builds a Carousel response from a list of JSON items.*/
function carouselBuilder(conv, carouselItems){
  // [SELECTION_KEY_ONE]: {
  //     synonyms: [
  //       'synonym of title 1',
  //       'synonym of title 2',
  //       'synonym of title 3',
  //     ],
  //     title: 'Title of First Carousel Item',
  //     description: 'This is a description of a carousel item.',
  //     image: new Image({
  //       url: IMG_URL_AOG,
  //       alt: 'Image alternate text',
  //     }),
  //   },

  var itemArray = {};

  // console.log('Carousel Items: ',carouselItems);
  conv.data.list_ids = {};

  for (n = 0; n < carouselItems.length; n++){
    let data = carouselItems[n].data();
    let item = {
      // title: `${i.Title} (${i.Released.slice(i.length-4,i.length)})`, // Finding Nemo (2008)
      title: `${data.title} (${data.year})`, // Finding Nemo (2008)
      description: data.plot,
      image: new Image({
        url: data.poster,
        alt: `Poster for ${data.title}`,
      })
    }
    itemArray[[data.title]] = item;
    conv.data.list_ids[data.title] = data.imdbId;
  }
  conv.data.list_items = itemArray;
  // console.log({items:itemArray});
  return new Carousel({items:itemArray});

}

/** Builds a list of movie items using JSON.*/
function movieListBuilder(conv, list_title, list_items){
  console.log('Building movie list');

  built_items = {};
  conv.data.list_ids = {};

  // description : `Added by: ${getUserName(conv)}  \n`+ list_items[n].plot.slice(0,100)+'...',
  for (n = 0;n < list_items.length;n++){
    let m = list_items[n];
    let item = {
      title       : m.title + ` (${m.year[0]})`,
      description : `Director: ${m.director} | Genre(s): ${m.genre.join(', ')} | IMDb Rating: ${m.imdbRating}  \n` + m.plot.slice(0,100)+'...',
      synonyms    : [m.title],
      image       : new Image({
        url : m.poster,
        alt : `Poster for ${m.title}`,
      })
    }
    // let key = [list_items[n].title];
    // built_items[key] = item;
    built_items[[m.title]] = item;
    conv.data.list_ids[m.title] = m.imdbId;
    // built_items[[String(n)]] = item;
  }

  conv.data.list_items = built_items;
  conv.data.list_details = {title : list_title, items: built_items};
  if (conv.data.response_stack){
    conv.data.response_stack.push({type:'list',title : list_title, items: built_items});
  } else {
    conv.data.response_stack = [{type:'list',title : list_title, items: built_items}];
  }
  // console.log({title : list_title, items: built_items});
  return new List({title : list_title, items: built_items});
}

// GROUPS

/** Changes the users current active group.*/
async function changeActiveGroup(conv,newName,newId){

  conv.user.storage.curr_group_id   = newId;
  conv.user.storage.curr_group_name = newName;

  var user_update = await db.collection('users').doc(conv.user.storage.user_id).update(
    {
      curr_group_name: newName,
      curr_group_ref: db.collection('groups').doc(newId),
      groups: admin.firestore.FieldValue.arrayUnion(db.collection('groups').doc(newId))
    }
  );

  var group_update = await db.collection('groups').doc(newId).update(
    {
      users: admin.firestore.FieldValue.arrayUnion(db.collection('users').doc(conv.user.storage.user_id))
    }
  );

}

/** Leaves the users current group and changes their active group to the next available.*/
async function leaveGroup(conv){
  console.log('leaveGroup helper triggered.');

  let userRef  = db.doc('users/' +conv.user.storage.user_id);
  let groupRef = db.doc('groups/'+conv.user.storage.curr_group_id);

  console.log('groupRef',typeof groupRef);


  var groupsArrayDeletion = await userRef.update(
    // Delete group from user's record
    'groups', admin.firestore.FieldValue.arrayRemove(groupRef)
  );
  // Get user document
  var userDoc = await userRef.get();
  // Get groups array
  var groups = userDoc.get('groups');
  // If no more groups, change current group params to empty strings
  if (groups.length === 0){
    let currGroupsDeletion = await userRef.update({curr_group_name:'',curr_group_ref:''});
  } else {
      // Else, set current groups to next group in array
      let newGroup = await groups[0].get();
      let newName = newGroup.get('name');
      let groupChange = await changeActiveGroup(conv,newName,groups[0].id);

      // await userRef.update({curr_group_ref:groups[0]});
      // await userRef.update({curr_group_name:newName});
  }


  var userDeletion = await groupRef.update(
    // Remove user from group's record
    'users', admin.firestore.FieldValue.arrayRemove(userRef)
  );
}

/** Creates a group summary car response.*/
async function groupSummaryCard(conv){
  var group_name = conv.user.storage.curr_group_name;
  var group_id   = conv.user.storage.curr_group_id;

  var groupDoc         = await db.doc('groups/'+group_id).get();
  var user_refs_array  = groupDoc.get('users');
  var users_array      = [];
  var user_names_array = [];
  var watchlist_array  = groupDoc.get('watchlist');
  var watchlist_length = (watchlist_array === undefined ? 0 : watchlist_array.length);
  var password         = groupDoc.get('passphrase');


  var docs = await db.getAll(user_refs_array);
  var all_rated = [];
  for (var x = 0; x < docs.length;x++){
    // console.log('NAME: ', docs[x].get('name'));
    var data = docs[x].data();
    var name = data.name;
    user_names_array.push(name.split(' ')[0]);
    all_rated = all_rated.concat(data.rated);
  }

  return new BasicCard(
    {
      text: ` **👨‍👨‍👧‍👦Members:** ${user_names_array.join(', ')}  \n
      **📜 Watchlist:** ${watchlist_length} movies  \n
      **⭐ Movies Rated:** ${all_rated.length}  \n
      **🔑 Passphrase:** ${password}`,
      subtitle: group_name,
      title: 'Group Summary',
    }
  );
}

/** Checks if a user is in an active group.*/
function hasActiveGroup(conv){
  const name = conv.user.storage.curr_group_name;
  return name !== '';
}


// WELCOME
/*
* Welcome + Authentication Intents
* ---------------
* - Default Welcome Intent
* - Get Signin
*/

app.intent('Default Welcome Intent', async (conv,params) => {
  console.log('Welcome intent triggered.');

  let reset_id        = conv.user.storage.user_id;
  let curr_group      = conv.user.storage.curr_group_id;
  let curr_group_name = conv.user.storage.curr_group_name;
  if (reset_id === 'd3lEK8FhXHdv8EOtMdI81TI1Zbp2'){
    conv.user.storage                 = {};
    conv.user.storage.user_id         = reset_id;
    conv.user.storage.curr_group_id   = curr_group;
    conv.user.storage.curr_group_name = curr_group_name;
  }

  const {payload} = conv.user.profile;
  if (payload) {
    // User account is linked
    // Start response stack
    conv.data.response_stack = [];
    const groupname = conv.user.storage.curr_group_name;

    conv.ask(new SimpleResponse({
      speech:`Welcome, ${payload.given_name}! You are currently in the group ${groupname}. Check out today's recommendations, or have a look at your group's watchlist.`,
      text:`Welcome, ${payload.given_name}! ✨ You are currently in the group ${groupname}. Try checking out today's recommendations, or have a look at your group's watchlist.`,
    }));
    let user_doc = await db.doc('users/'+conv.user.storage.user_id).get();
    conv.user.storage.rated = user_doc.get('rated');
    if (conv.user.storage.rated === undefined || conv.user.storage.rated.length < 10){
      let card = await groupSummaryCard(conv);
      conv.ask(new Suggestions(['Quick Ratings ⚡','Top Recommendations 🎥','Movie Match 🕵🏽','Show our watch list 🍿','Group Summary 📑','List Groups 📜','Help ❓']));
      conv.ask(new SimpleResponse({
        speech:`You haven't rated many movies yet, ${getUserName(conv)}. Ask for "Quick ratings" to quickly beef up your ratings profile.`,
        text:`You haven't rated many movies yet, ${getUserName(conv)}, so your recommendations might not be personalied.  ` +
        `Ask for "Quick ratings" to quickly beef up your ratings profile.`
      }));

      return conv.ask(card);
    } else {
      let card = await groupSummaryCard(conv);
      conv.ask(new Suggestions(SUGGS['welcome']));
      return conv.ask(card);
    }
  } else {
  // User has not linked account
  return conv.ask(new SignIn('Welcome to Movie Match. Here you can get movie recommendations\
     for you and your friends.  In order to form a group and start getting recommendations'));
  }
});

/** Receive Sign in with Google Sign-In */
app.intent('Get Signin', async (conv, params, signin) => {
  if (signin.status !== 'OK') {
    return conv.close('Let\'s try again another time.');
  }
  const {email} = conv.user;
  if (!conv.data.uid && email) {
    try {
      conv.data.uid = (await auth.getUserByEmail(email)).uid;
    } catch (e) {
      if (e.code !== 'auth/user-not-found') {
        throw e;
      }
      // If the user is not found, create a new Firebase auth user
      // using the email obtained from the Google Assistant
      conv.data.uid = (await auth.createUser({email})).uid;
    }
  }
  if (conv.data.uid) {
    conv.user.storage.user_id = conv.data.uid;
    let create_user = await db.collection('users').doc(conv.data.uid).set(
      {
        name    : conv.user.profile.payload.name,
        created : admin.firestore.Timestamp.now()
      }
    );
  }
  const payload = conv.user.profile.payload;
  var password  = await passwordGenerator();
  var groupName = `${payload.given_name}'s Group`;
  let docRef    = await db.collection('groups').add(
    {
      name: groupName,
      users:[db.collection('users').doc(conv.user.storage.user_id)],
      passphrase: password,
      created: admin.firestore.Timestamp.now()
    }
  );

  conv.user.storage.curr_group_name = groupName;
  conv.user.storage.curr_group_id   = docRef.id;
  conv.data.response_stack          = [];

  var update = await db.collection('users').doc(conv.user.storage.user_id).update({
    curr_group_name: groupName,
    curr_group_ref: docRef,
    groups: admin.firestore.FieldValue.arrayUnion(docRef)
  });

  conv.ask(new SimpleResponse({
    speech:`Thanks for signing in, ${payload.given_name}. To get you started using Movie Match, I've created a new group for you. `
            + 'Feel free to make a head-start '
            + 'on rating some movies so that your recommendations can become personalised! '
            + 'To quickly build up your profile, you should ask for "Quick Ratings". '
            + `What would you like to get started with?`,
    text:`Thanks for signing in, ${payload.given_name}. To get you started using Movie Match, I've created a new group for you. `
            + 'To have a friend join your group, '
            + 'they should ask to join a group and quote the passphrase below. Feel free to make a head-start '
            + 'on rating some movies so that your recommendations can become more personalised! '
            + 'To quickly build up your ratings profile, ask for "Quick Ratings". '
            + `What would you like to do next?`,
  }));

  conv.ask(new Suggestions(['Quick Ratings ⚡','Rename Group 📝','Join Group 🎉','Show me some movies 🎥','Help ❓']));
  return conv.ask(new BasicCard({text:`**${password}**`,title: `Group Passphrase 🔒`,subtitle:`Quote this passphrase to join ${groupName}`}));
});

// GROUPS
/*
* GROUP INTENTS
* ---------------
* - Create Group
* - Name Group
* - Perform Naming
* - Rename Group
* - Get Current Group
* - List Groups
* - Change Current Group
* - Leave Group
* - Invite to Group
* - Group Summary
* - Join Group
* - Receive Password
*/

// Creates a new group for the user
app.intent('Create Group', async (conv, params) => {
  console.log('Create Group intent triggered.')
  var colRef    = db.collection('groups');
  var userRef   = db.collection('users').doc(conv.user.storage.user_id);
  var groupName = selectName();
  var password  = await passwordGenerator();

  /*Need to return result of async call to resolve promise*/
  return colRef.add({name: groupName,users:[db.collection('users').doc(conv.user.storage.user_id)],passphrase: password,created:admin.firestore.Timestamp.now()}).then(docRef => {
    console.log('Added document with ID: ', docRef.id,' and name: ', groupName);

    conv.user.storage.curr_group_name = groupName;
    conv.user.storage.curr_group_id   = docRef.id;

    context_parameters = {
      groupname : `${groupName}`,
      groupid   : `${docRef.id}`
    }

    var update = db.collection('users').doc(conv.user.storage.user_id).update({
      curr_group_name: groupName,
      curr_group_ref: docRef,
      groups: admin.firestore.FieldValue.arrayUnion(docRef)
    });

    // conv.contexts.set('expecting-rename-answer', 1, context_parameters);
    const responseString = 'New group "' + groupName + '" created. To have a friend join your group,' +
                           'they should quote the passphrase below. Feel free to make a head' +
                           'start on rating some movies in order to start receiving recommendations for' +
                           'your group!';


    conv.ask(responseString);
    conv.ask(new BasicCard({text:`**${password}**`,title: `Group Passphrase 🔒`,subtitle:`Quote this passphrase to join ${groupName}`}));
    return conv.ask(new Suggestions(['Rename Group 📝', 'Invite to Group 📨','List Groups 📜','Join Group 🎉','Show me some movies 🎥', 'Help ❓']));
  }).catch(error => {
    /* Can log error and stay in intent, or */
    console.error(`Error adding doc: ${error}`);
    conv.ask('Failed to create a new group.');
    /*Throw error to be caught by app.catch*/
    // throw Error(error);
  });
});

// Takes the request to name a group, and asks the user for confirmation
app.intent('Name Group', (conv, params) => {
  console.log('Name Group Intent triggered.');
  let confirmation = `You are currently in the group ${conv.user.storage.curr_group_name}. Would you like to rename this group?`
  conv.ask(new Suggestions(SUGGS['yes or no']));
  return conv.ask(new Confirmation(confirmation));
});

// Receive a new name and perform the naming within Firestore
app.intent('Perform Naming', async (conv, params) => {
  console.log('Perform Naming intent triggered.');

  // Get given group name from params
  var newName = params.groupName;

  // Set it in firebase
  let groupRef = db.doc('groups/' + conv.user.storage.curr_group_id);
  let userRef  = db.doc('users/' + conv.user.storage.user_id);

  try {
    var updateRes  = groupRef.update({name: newName});
    var updateRes2 = userRef.update({curr_group_name: newName});
    conv.user.storage.curr_group_name = newName;
    console.log(`Document updated at ${updateRes.updateTime}`);
    conv.ask(new Suggestions(SUGGS['default']));
    return conv.ask(`Okay, the new group name is ${conv.user.storage.curr_group_name}! What would you like to do next?`);
  } catch(error) {
    console.error(`Error renaming group doc: ${error}`);
    return defaultAsk(conv,'Failed to rename group. Please try again later.');
  }

});

// Lets the user know which group they are currently active in
//TODO deprecate in favour of group summary
app.intent('Get Current Group', async (conv,params) => {
  console.log('Get Current Group Intent triggered.');

  const storedGroupName    = conv.user.storage.curr_group_name;
  const userDoc            = await db.doc('/users/'+ conv.user.storage.user_id).get();
  const firestoreGroupName = userDoc.data().curr_group_name;

  if (storedGroupName === firestoreGroupName){
    return defaultAsk(conv,`You are currently in the group ${storedGroupName}.`);
  } else {
    conv.user.storage.curr_group_name = firestoreGroupName;
    return defaultAsk(conv,`You are currently in the group ${firestoreGroupName}.`);
  }

});

// Lists the groups that the user is a member of
app.intent('List Groups', async (conv,params) => {
  console.log('List Groups Intent triggered.');


  const userDocRef = db.doc('/users/' + conv.user.storage.user_id);
  // const userDoc = await db.doc('/users/' + conv.user.storage.user_id).get();
  // const usersGroups = userDoc.data().groups
  var groupNames = [], groupDocs = [];

  return db.collection('groups').where('users','array-contains',userDocRef).get().then(r=> {
    if (r.empty){
      return defaultAsk(conv,`You are not in any groups yet, ${getUserName(conv)}. Try creating one for yourself, or ask a friend to invite you!`);
    } else if (r.docs.length === 1){
      return defaultAsk(conv,`You are only in one group, ${conv.user.storage.curr_group_name}`);
    } else {
      r.forEach(n => {
        // console.log('n ',n);
        if (n.data().name === conv.user.storage.curr_group_name){
          groupNames.push(n.data().name + ' (Active)');
        } else {
          groupNames.push(n.data().name);
        }
      });
      const listObject = listBuilder('Your Groups',groupNames);
      conv.contexts.set('choosing_group',1);
      conv.ask('Here are all your groups.  Select a group to change your active group!');
      conv.ask(new Suggestions(SUGGS['default']));
      return conv.ask(listObject);
    }
  });
});

// Changes the user's current group
app.intent('Change Current Group', async (conv,params) => {
  console.log('Change Current Group Intent triggered.');
  console.log('Params:', params);
  console.log('conv.data.chosen_group: ', conv.data.chosen_group);


  let oldGroup   = conv.user.storage.curr_group_name;
  let newGroup   = params.group_name;

  // const context = conv.contexts.get('group_chosen');
  if (conv.data.chosen_group){
    newGroup = conv.data.chosen_group;
    console.log('Group chosen from conv data: ', newGroup);
    conv.data.chosen_group = {};
  }
  let userDocRef = db.collection('users').doc(conv.user.storage.user_id);
  var newId      = "";

  if (newGroup === '') {
    console.log('No group param or empty string.');
    return conv.followup('list_groups_event',{});
  }

  return db.collection('groups').where('users','array-contains',userDocRef).where('name','==',newGroup).get().then(r=> {
    if (r.empty){
      return conv.followup('list_groups_event',{});
    } else {
      let doc   = r.docs[0];
      let newId = doc.id;
      console.log(newId);

      changeActiveGroup(conv,newGroup,newId);
      return defaultAsk(conv,`Okay, changing your current group from ${oldGroup} to ${newGroup}.`)
    }
  });
});

// Leave the current group
app.intent('Leave Group', async (conv,params) => {
  console.log('Leave Group intent triggered.');
  conv.contexts.set('leaving_group',1,{});
  conv.ask(new Suggestions(SUGGS['yes or no']));
  conv.ask(new Confirmation(`Are you sure you want to leave the group ${conv.user.storage.curr_group_name}? The group will be deleted if all members leave.`));
});

// Get the group password card to send to a friend
app.intent('Invite to Group', async (conv,params) => {
  // https://assistant.google.com/services/invoke/uid/000000151635bf58
  console.log('Invite to Group Intent triggered.');

  if (!hasActiveGroup(conv)){
    return defaultAsk(conv,'You are not currently in any groups.');
  }

  let groupDoc  = await db.doc('groups/'+conv.user.storage.curr_group_id).get();
  let groupName = conv.user.storage.curr_group_name;
  let password  = groupDoc.get('passphrase')

  conv.ask('Send this password to your friends to invite them to your group! They just need to '
          +'ask Movie Match to join a group and quote this passphrase. Please note that in a Movie Match group,'
          +'every member has equal privileges, and will be included in the group recommendations.');

  return defaultAsk(conv,new BasicCard({text:`**${password}**`,title: `Group Passphrase 🔒`,subtitle:`Quote this passphrase to join ${groupName}`}));
});

// Get a group summary
app.intent('Group Summary', async (conv,params) => {
  console.log('Group Summary intent triggered');

  var group_name = conv.user.storage.curr_group_name;
  var group_id   = conv.user.storage.curr_group_id;

  var groupDoc         = await db.doc('groups/'+group_id).get();
  var user_refs_array  = groupDoc.get('users');
  var users_array      = [];
  var user_names_array = [];
  var watchlist_array  = groupDoc.get('watchlist');
  var watchlist_length = (watchlist_array === undefined ? 0 : watchlist_array.length);
  // var watchlist_string = watchlist_array.slice(0,3).join(', ');
  var password         = groupDoc.get('passphrase');


  var docs = await db.getAll(user_refs_array);
  for (var x = 0; x < docs.length;x++){
    // console.log('DOC: ',docs[x]);
    console.log('NAME: ', docs[x].get('name'));
    var data = docs[x].data();
    var name = data.name;
    user_names_array.push(name.split(' ')[0]);
  }
  console.log(user_names_array);

  conv.ask(`Here is a quick summary of your group ${conv.user.storage.curr_group_name}:`);
  return defaultAsk(conv,new BasicCard(
    {
      text: ` **👨‍👨‍👧‍👦Members:** ${user_names_array.join(', ')}  \n
      **📜Watchlist:** ${watchlist_length} movies  \n
      **❤️Favourite Genre:** None  \n
      **🔑Passphrase:** ${password}`,
      subtitle: group_name,
      title: 'Group Summary',
    }
  ));
});

// Ask to input a password to join a group
app.intent('Join Group', async (conv,params) => {
  // TODO Make sure Cancel works alright here, and in rename, add/remove watchlist, and BACK when in lists
  console.log('Join Group intent triggered.');

  //TODO a nice card responseeee
  conv.ask('Please provide the passphrase for the group you would like to join!');
});

// Get the password from the user
app.intent('Receive Password', async (conv,params,input) => {
  console.log('Receive Password intent triggered.');

  var password = params['password'];
  if (!password){
    password = input;
    console.log('Password taken from raw input: ', password);
  }
  console.log('Password: ', password);



  var querySnapshot = await db.collection('groups').where('passphrase','==',password).get();

  if (querySnapshot.empty){
    return defaultAsk(conv,'This password does not match an open group - please check the provided phrase and try again!');
  } else {
    if (querySnapshot.docs[0].get('users').includes(db.collection('users').doc(conv.user.storage.user_id))){
      return defaultAsk(conv,'You are already a member of this group!');
    }
    var group_name = querySnapshot.docs[0].get('name');
    var group_id   = querySnapshot.docs[0].id;
  }

  try {
    changeActiveGroup(conv,group_name,group_id);
  } catch (e) {
    console.log('Error joining group ', group_name, group_id);
  }

  //TODO A fun card response with group summary
  return defaultAsk(conv,`You are now in the group ${group_name}. Congrats! 🎉`);
});


// WATCH LIST
/*
* Watch List Intents
* ---------------
* - Get Watch List
* - Add to Watch list
* - Add Card to Watch List
* - Remove from list
*/

// Get's the user's current group's watchlist
app.intent('Get Watch List', async (conv, params) => {
  console.log('Get Watch List intent triggered.');

  if (!hasActiveGroup(conv)){
    return defaultAsk(conv,'You are not currently in any groups.');
  }

  if (!conv.user.profile){
    return defaultAsk(conv,'You have not linked your account.');
  }
  conv.ask('Here\'s your group\'s watch list. Anyone can add to or remove from the list.');
  return await watchlistHelper(conv);
});

// Adds a movie specified by name to the watchlist
app.intent('Add to Watch List', async (conv, params) => {
  console.log('Add to Watch List Intent triggered.');

  var movieToAdd = params.movie;

  var groupRef   = await db.doc('groups/'+conv.user.storage.curr_group_id);
  var groupDoc   = await groupRef.get();
  var storedlist = groupDoc.data().watchlist;
  var imdb_id;

  let snap = await db.collection('movies').where('title','==',movieToAdd).get();
  if (snap.empty){
    console.log('Snap empty');
    // do search?
    let movie_url_safe = encodeURIComponent(movieToAdd);
    let string         = `https://api.themoviedb.org/3/search/movie?api_key=ec080b067cda6ab8ab01a853231a54a4&language=en-US&query=${movie_url_safe}&page=1&include_adult=false`;
    let response       = await axios.get(string);
    // console.log(string);
    // console.log(response);
    if (response.data.total_results === 0){
      return defaultAsk(conv,`Could not find a movie named ${movieToAdd}. Please try again`);
    }
    let tmdb_id = response.data.results[0].id;
    movieToAdd  = response.data.results[0].title;
    response    = await axios.get(`https://api.themoviedb.org/3/movie/${tmdb_id}/external_ids?api_key=ec080b067cda6ab8ab01a853231a54a4`);
    imdb_id     = response.data.imdb_id;
  } else {
    console.log('Snap not empty');
    let docs = snap.docs;
    imdb_id  = docs[0].get('imdbId');
  }

  // {title,added_by,desc,imdbId,poster}

  try{
    if (typeof(storedlist) === 'undefined'){
      var add = await groupRef.set({watchlist: [imdb_id]}, {merge: true});
    } else {
      var update = await groupRef.update({watchlist: admin.firestore.FieldValue.arrayUnion(imdb_id)});
    }
    console.log(`${imdb_id} was added to the group's watchlist.`);
    conv.ask(`${movieToAdd} was added to the group's watchlist.`);
    return await watchlistHelper(conv);
  } catch(e){
    console.log(e);
    return defaultAsk(conv,'Unable to add movie to watchlist. Please try again later.');
  }
});

// Adds the current movie card to the watchlist
app.intent('Add Card to Watch List', async (conv,params) => {
  console.log('Add Card to Watch List intent triggered.');

  let context = conv.contexts.get('movie_card_details');
  let imdb_id = context.parameters.imdbId;
  let c_title = context.parameters.title;

  var response = `${c_title} was added to the group's watchlist.`;

  let rapid_ratings = conv.contexts.get('rapid_ratings');
  if (rapid_ratings){
    let cparams       = rapid_ratings.parameters;
    let movie_count   = cparams.movie + 1;

    response = `${c_title} was added to the group's watchlist. Come back and tell me what you thought once you've watched it!`;
    conv.contexts.set();

    if (movie_count < 9){
      conv.ask(response);
      conv.ask(new Suggestions(SUGGS['quick ratings']));
      conv.contexts.set('rapid_ratings',1,{name:cparams.name,list: cparams.list,movie: movie_count});
      const card_obj = await buildMovieCard(conv, cparams.list[movie_count]);
      return conv.ask(card_obj);
    } else {
      conv.user.storage.completed_lists = cparams.name;
      conv.ask(new Suggestions(SUGGS['default']));
      conv.ask('Thanks for rating those! Your movie recommendations should improve soon. If you\'d like to help out with more ratings, we can go through another list.');
      return conv.ask('What would you like to do next?');
    }
  }

  let groupRef = db.doc('groups/'+conv.user.storage.curr_group_id);
  let update   = await groupRef.update({watchlist: admin.firestore.FieldValue.arrayUnion(imdb_id)});

  conv.contexts.set('movie_card_details',2,{title:c_title,imdbId:imdb_id}),

  console.log(`${imdb_id} was added to the group's watchlist.`);
  conv.ask(`${c_title} was added to the group's watchlist.`);
  conv.ask(new Suggestions(SUGGS['ratings']));
  let movieCard = await buildMovieCard(conv,imdb_id);
  return conv.ask(movieCard);
});

// Removes a movie by name or card from the watchlist
app.intent('Remove from Watch List', async (conv, params) => {
  console.log('Remove from Watch List Intent triggered.');

  var watchlist_context = conv.contexts.get('viewing_watchlist');
  var movieToRemove, movieTitle;
  if (watchlist_context){
    movieToRemove = watchlist_context.parameters.movie_to_remove;
    movieTitle    = watchlist_context.parameters.title;
  }

  var groupRef = db.doc('groups/'+conv.user.storage.curr_group_id);
  var groupDoc = await groupRef.get();
  var storedlist = groupDoc.data().watchlist;

  try{
    if (typeof(storedlist) === 'undefined' || storedlist.length === 0){
      return defaultAsk(conv,'Your watchlist is already empty!');
    } else {
      var remove = await groupRef.update({watchlist: admin.firestore.FieldValue.arrayRemove(movieToRemove)});
      conv.ask(`${movieTitle.slice(0, movieTitle.length-7)} was removed from the group's watchlist.`);
      return watchlistHelper(conv);
    }
  } catch(e){
    console.log(e);
    return defaultAsk(conv,'Unable to remove movie from watchlist. Please try again later.');
  }
});

/*
* Movie Intents
* ---------------
* - Search Movies
* - Show Movies
* - More Movies
* - Rate Movie
* - Rate Movies - Rapid
* - Rapid Ratings
* - Rapid Ratings - skip
* - Rapid Ratings - cancel
* - Go Back
* - Similar Movies
* - Similar Movies
* - Get Movie Recommendations
*/


// MOVIES

// Asks for a search term to look for movies in the database
app.intent('Search Movies', async (conv,params) => {
  console.log('Search Movies intent triggered');

  conv.contexts.set('awaiting_search_term',3,{});
  conv.ask(new Suggestions(SUGGS['search']));
  return conv.ask(new SimpleResponse({
    speech: 'What kind of movie are you in the mood for?',
    text: 'What kind of movie are you in the mood for?  Hint: Would you like something scary, or something exciting?',
  }));
});

// Shows a list of 10 movies from the database, either ordered by IMDb ranking or by genre and ranking
app.intent('Show Movies', async (conv, params) => {
  console.log('Show Movies intent triggered.');

  let context            = conv.contexts.get('awaiting_search_term');
  if (context){
    var userGenre          = context.parameters.movieGenre;
    var originalPhrase     = context.parameters['movieGenre.original'];
  }
  // originalPhrase         = originalPhrase.replace(/[.,/#!$%^&*;:{}=\-_`~()]/g,"");
  var movie_list_context = conv.contexts.get('movie_list_details');

  var query, response;

  var list_title = `Popular Movies for ${conv.user.storage.curr_group_name}🍿`;
  if (typeof userGenre === 'undefined' || userGenre === '') {
    console.log('Undefined genre.')
    // query = db.collection('movies')
    //                         .where('released','>', admin.firestore.Timestamp.fromDate(new Date('01/01/2017')))
    //                         .orderBy('released','desc')
    //                         .limit(20);
    query = db.collection('movies')
                            .where('rankedRating','>',6)
                            .orderBy('rankedRating','desc')
                            .limit(20);

    response = 'Here are some popular movies your group might like.';
  } else {
    console.log('Genre: ', userGenre);
    list_title = `${userGenre} Movies for ${conv.user.storage.curr_group_name}🍿`;
    query = db.collection('movies')
                            .where('rankedRating','>',6)
                            .where('genre','array-contains',userGenre)
                            .orderBy('rankedRating','desc')
                            .limit(20);
    response = `Here are some ${originalPhrase} movies your group might enjoy.`;
  }

  let querySnapshot = await query.get();
  console.log('Query docs length: ', querySnapshot.docs.length);

  var already_rated = conv.user.storage.rated;
  var list_items = [];
  if (!querySnapshot.empty){
    for (let doc of querySnapshot.docs){
      if (already_rated){
        if (already_rated.includes(doc.get('imdbId'))){
          console.log('Already rated',doc.get('imdbId'));
        } else {
          list_items.push(doc.data());
        }
      } else {
        list_items.push(doc.data());
      }
    }
  } else {
    return defaultAsk(conv,'Couldn\'t find any movies!.');
  }
  list_items = list_items.slice(0,10);
  const listObj   = movieListBuilder(conv, list_title, list_items)
  const last_seen = querySnapshot.docs[querySnapshot.docs.length-1];

  conv.contexts.set('movie_list_details',3,{
    genre:userGenre,
    start_at:0,
  });
  // carousel:carouselObj

  conv.ask(response);
  conv.ask(new Suggestions(SUGGS['recommendations']));
  return conv.ask(listObj);
});

// Gets the next 10 movies from the database
app.intent('More Movies', async (conv,params) => {
  console.log('More Movies intent triggered.');

  // {
  //   genre:context.parameters.genre,
  //   start_at:context.parameters.start_at+10,
  //   docs:context.parameters.docs,
  //   list:context.parameters.list
  // });
  const context   = conv.contexts.get('movie_list_details');
  const userGenre = context.parameters.genre;
  var start_at    = context.parameters.start_at + 10;
  if (!start_at){
    start_at = 0;
  }

  var query;
  if (userGenre === '') {
    query = db.collection('movies')
              .orderBy('rankedRating','desc')
              .limit(10)
              .offset(start_at);
  } else {
    query = db.collection('movies')
              .where('rankedRating','>',6)
              .where('genre','array-contains',userGenre)
              .orderBy('rankedRating','desc')
              .limit(10)
              .offset(start_at);
  }

  let querySnapshot = await query.get();
  console.log('Query docs length: ', querySnapshot.docs.length);

  var list_items = [];
  if (!querySnapshot.empty){
    for (let doc of querySnapshot.docs){
      list_items.push(doc.data());
    }
  } else {
    return defaultAsk(conv,'Couldn\'t find any movies!.');
  }

  const listObj   = movieListBuilder(conv, conv.data.list_details.title, list_items);
  const last_seen = querySnapshot.docs[querySnapshot.docs.length-1];

  conv.contexts.set('movie_list_details',3,{genre:userGenre, start_at:start_at});

  conv.ask(new Suggestions(SUGGS['recommendations']));
  conv.ask(`Here are some more movies to try out.`);
  return conv.ask(listObj);

});

// Submits a rating for a movie
app.intent('Rate Movie', async (conv,params) => {
  console.log('Rate Movie intent triggered.');

  var context  = conv.contexts.get('movie_card_details');
  var movie    = context.parameters.title;
  var movie_id = context.parameters.imdbId;
  var rating   = parseInt(params.rating);

  if (!context){
    return conv.followup('show_movie_event');
  }

  // console.log(movie,movie_id,rating);

  // let usrRating = await db.doc('users/' + conv.user.storage.user_id + '/ratings/' + movie_id).set({
  //   title: movie,
  //   rating: rating,
  //   timestamp: admin.firestore.Timestamp.now()
  // });
  //
  // let colRating = await db.collection('ratings').add({
  //   imdbId: movie_id,
  //   rating: rating,
  //   timestamp: admin.firestore.Timestamp.now(),
  //   userId: conv.user.storage.user_id,
  // });

  let submit = await submitRating(conv,movie_id,rating,movie);

  var response = '';
  if (rating === 1) {
    // response += '1 star. Yikes! 💩'; // Poop emoji
    response = new SimpleResponse({
      speech: `You rated ${movie} 1 star. Yikes!`,
      text: `You rated ${movie} 1 star. Yikes! 💩`,
      });
  } else if (rating === 2) {
    // response += '2 stars. Too bad! 🤷'; // Shrug emoji
    response = new SimpleResponse({
      speech: `You rated ${movie} 2 stars. Too bad!`,
      text: `You rated ${movie} 2 stars. Too bad! 🤷`,
      });
  } else if (rating === 3) {
    // response += '3 stars. Not bad! 🙂'; // Smiling emjoji
    response = new SimpleResponse({
      speech: `You rated ${movie} 3 stars. Not bad!`,
      text: `You rated ${movie} 3 stars. Not bad! 🙂`,
      });
  } else {
    // response += (rating + ' stars. Amazing! 🌟'); // Shining star emoji
    response = new SimpleResponse({
      speech: `You rated ${movie} ${rating} stars. Amazing! `,
      text: `You rated ${movie} ${rating} stars. Amazing! 🌟`,
      });
  }

  conv.data.response_stack.pop(); // Pop last card from response stack so it's not there twice
  conv.ask(response);
  conv.ask(new Suggestions(SUGGS['ratings']));
  let movieCard = await buildMovieCard(conv,movie_id);
  return conv.ask(movieCard);

});

// Submits a rating for a movie while within the quick ratings flow
app.intent('Rate Movie - rapid', async(conv,params) => {
  console.log('Rate Movie - rapid intent triggered');

  var context  = conv.contexts.get('movie_card_details');
  var movie    = context.parameters.title;
  var movie_id = context.parameters.imdbId;
  var rating   = parseInt(params.rating);

  let submit = submitRating(conv,movie_id,rating,movie);

  var response = '';
  if (rating === 1) {
    response = new SimpleResponse({
      speech: `You rated ${movie} 1 star. Yikes!`,
      text: `You rated ${movie} 1 star. Yikes! 💩`,
      });
  } else if (rating === 2) {
    response = new SimpleResponse({
      speech: `You rated ${movie} 2 stars. Too bad!`,
      text: `You rated ${movie} 2 stars. Too bad! 🤷`,
      });
  } else if (rating === 3) {
    response = new SimpleResponse({
      speech: `You rated ${movie} 3 stars. Not bad!`,
      text: `You rated ${movie} 3 stars. Not bad! 🙂`,
      });
  } else {
    response = new SimpleResponse({
      speech: `You rated ${movie} ${rating} stars. Amazing! `,
      text: `You rated ${movie} ${rating} stars. Amazing! 🌟`,
      });
  }

  let rapid_ratings = conv.contexts.get('rapid_ratings');
  let cparams       = rapid_ratings.parameters;
  let movie_count   = cparams.movie + 1;

  if (movie_count < 9){
    conv.ask(response);
    conv.ask(new Suggestions(SUGGS['quick ratings']));
    conv.contexts.set('rapid_ratings',1,{name:cparams.name,list: cparams.list,movie: movie_count});
    const card_obj = await buildMovieCard(conv, cparams.list[movie_count]);
    return conv.ask(card_obj);
  } else {
    conv.user.storage.completed_lists = cparams.name;
    conv.ask(new Suggestions(SUGGS['default']));
    conv.ask('Thanks for rating those! Your movie recommendations should improve soon. If you\'d like to help out with more ratings, we can go through another list.');
    return conv.ask('What would you like to do next?');
  }
});

// Starts a quick ratings flow
app.intent('Rapid Ratings', async(conv,params) => {
  console.log('Rapid Ratings intent triggered');

  let completed_lists = [];
  if (conv.user.storage.completed_lists){
    completed_lists = conv.user.storage.completed_lists;
  }

  let lists = await db.collection('movielists').get();
  var movie_list,list_name;
  for (let list of lists.docs){
    if (!completed_lists.includes(list.id)){
      list_name  = list.id;
      movie_list = list.get('movies');
      break;
    }
  }

  if (completed_lists.length === 2){
    conv.ask('You\'ve rated all the lists I curated for you... well done!');
    return defaultAsk('What would you like to do now?');
  }

  var first_movie = movie_list[0];
  var i = 0;
  if (conv.user.storage.rated){
    while (conv.user.storage.rated.includes(first_movie)){
      console.log('Already rated',movie_list[i]);
      i++;
      first_movie = movie_list[i];
    }
  }

  console.log('Movie list:', movie_list);
  conv.ask('Lets quickly rate some movies to build up your recommendation profile.  The more movies you rate, the more accurate your recommendations will become.');
  conv.ask(new Suggestions(SUGGS['quick ratings']));
  conv.contexts.set('rapid_ratings',1,{name:list_name,list: movie_list,movie: i});
  const card_obj = await buildMovieCard(conv,first_movie);
  return conv.ask(card_obj);
});

// SKips a movie during a quick ratings flow
app.intent('Rapid Ratings - skip', async(conv,params) => {
  console.log('Skip Rapid Rating intent triggered');

  const context   = conv.contexts.get('rapid_ratings');
  var cparams     = context.parameters;
  var movie_count = cparams.movie + 1;

  if (movie_count < 9){
    conv.ask('Okay, let\'s skip that one.');
    conv.ask(new Suggestions(SUGGS['quick ratings']));
    conv.contexts.set('rapid_ratings',1,{name:cparams.name,list:cparams.list,movie:movie_count});
    const card_obj = await buildMovieCard(conv,cparams.list[movie_count]);
    return conv.ask(card_obj);
  } else {
    conv.user.storage.completed_lists = cparams.name;
    conv.ask('Thanks for rating those! Your movie recommendations should improve soon. If you\'d like to help out with more ratings, we can go through another list.');
    return defaultAsk(conv,'What would you like to do next?');
  }
});

// Cancels the quick ratings flow
app.intent('Rapid Ratings - cancel', async(conv,params) => {
  console.log('Rapid Ratings - stop intent triggered');

  conv.ask('Thanks for rating those! Your movie recommendations should improve soon.');
  return defaultAsk(conv,'What would you like to do next?');
});

// Go back to the previous response (card or list)
app.intent('Go Back', async (conv,params) => {
  console.log('Go Back intent triggered.');

  let list_details   = conv.contexts.get('movie_list_details');
  let went_back = conv.contexts.get('went_back');

  conv.data.response_stack.pop();
  var last_response = conv.data.response_stack[conv.data.response_stack.length-1];

  // // If going back for this first time
  // if (!went_back){
  //   // We need to pop twice to get the previous response
  //   last_response = conv.data.response_stack.pop();
  // }

  if (!last_response){
    conv.data.response_stack = [];
    return conv.followup('Welcome',{});
  }


  var stored_type  = last_response.type;
  var stored_title = last_response.title;
  if (stored_type === 'list'){

    var stored_items = last_response.items;

    conv.contexts.set('went_back',1,{});
    if (list_details){
      if (list_details.parameters.genre){
        conv.contexts.set('movie_list_details',3,{genre:list_details.parameters.genre, start_at:list_details.parameters.start_at});
      } else {
        conv.contexts.set('movie_list_details',3,{});
      }
    } else {
      conv.contexts.set('movie_list_details',3,{});
    }

    conv.ask('Sure, here\'s the previous items again.');
    if (stored_title.includes('If you liked')) {
      // The previous list was a similar movies list
      conv.ask(new Suggestions(SUGGS['similar']));
    } else if (stored_title.includes('Popular')){
      // The previous list was a list of database queried movies
      conv.ask(new Suggestions(SUGGS['recommendations']));
    } else {
      // The previous list was a fixed length list of recommendations (or another unkown type of list?)
      conv.ask(new Suggestions(SUGGS['default']));
    }
    return conv.ask(new List({title:stored_title, items:stored_items}));
  } else if (stored_type === 'card') {
    var stored_imdb  = last_response.imdb_id;
    var stored_card  = last_response.contents;

    conv.contexts.set('went_back',1,{});
    conv.contexts.set('movie_card_details',2,{title:stored_title,imdbId:stored_imdb});

    conv.ask('Sure, let\'s go back to the last movie again.');
    conv.ask(new Suggestions(SUGGS['ratings']));
    return conv.ask(new BasicCard(stored_card));
  }
  // let stored_title = conv.data.list_details.title;
  // let stored_items = conv.data.list_details.items;
  // let stored_items = conv.data.list_items;

  console.log('Nowhere to go back to?');
  return defaultAsk('I think we\'ve went back as far as we can!');
});

// Look for similar movies to the current card using TMDb
app.intent('Similar Movies', async (conv,params) => {
  console.log('Similar Movies intent triggered.');

  //TODO Word embedding or Content Based recommendations for this movie.

  let context     = conv.contexts.get('movie_card_details');
  let imdb_id     = context.parameters.imdbId;
  let movie_title = context.parameters.title;


  console.log('Searching for tmdb id');
  let query_id = await axios.get(`https://api.themoviedb.org/3/find/${imdb_id}?api_key=${tmdbApiKey}&language=en-US&external_source=imdb_id`);
  let tmdb_id  = query_id.data.movie_results[0].id;

  console.log('Searching for similar movies');
  let query_similar = await axios.get(`https://api.themoviedb.org/3/movie/${tmdb_id}/similar?api_key=${tmdbApiKey}&language=en-US&page=1`);
  let list_items    = query_similar.data.results.slice(0,7);

  console.log('Searching for imdb ids of similar movies');
  var promises = [];
  for (let item of list_items){
    promises.push(axios.get(`https://api.themoviedb.org/3/movie/${item.id}/external_ids?api_key=${tmdbApiKey}`));
  }
  var responses = await Promise.all(promises);

  var imdb_ids = []
  for (let item of responses){
    if (!item.data.status_code){
      imdb_ids.push(item.data.imdb_id);
    }
  }

  var already_rated = conv.user.storage.rated;
  var moviePromises = [];
  for (let id of imdb_ids){
    if (already_rated){
      if (!already_rated.includes(id)){
        moviePromises.push(db.collection('movies').doc(id).get());
      }
    } else {
      moviePromises.push(db.collection('movies').doc(id).get());
    }
  }
  let querySnaps = await Promise.all(moviePromises);

  var movieDocs = [];
  var missingMovies = [];
  for (let doc of querySnaps){
    if (doc.exists){
      movieDocs.push(doc.data());
    } else{
      missingMovies.push(fixMissingImdb(doc.id));
    }
  }

  if (missingMovies.length > 0){
    let fixedMovies = await Promise.all(missingMovies);
    movieDocs = movieDocs.concat(fixedMovies);
  }
  movieDocs = movieDocs.slice(0,5);

  let listObj = movieListBuilder(conv,`If you liked ${movie_title}, you might like...`,movieDocs);

  conv.contexts.set('movie_list_details',3,{genre:'', start_at:''});

  conv.ask(new Suggestions(SUGGS['similar']))
  conv.ask(`Here are some similar movies to ${movie_title}.`);
  return conv.ask(listObj);
});

// Gets recommendations from the App Engine recommendation service
app.intent('Get Movie Recommendations', async (conv) => {
  console.log('Get Movie Recommendations intent triggered.');

  let group_doc = await db.doc('groups/'+conv.user.storage.curr_group_id).get();
  let users = group_doc.get('users');
  var user_ids = [];
  for (let u in users){
    user_ids.push(users[u].id);
  }

  /*
  User has rated less than 10 movies. We should check that their group cumulatively has
  rated at least 10 movies before trying to provide them with personalised recommendations.
  */
  if (conv.user.storage.rated && conv.user.storage.rated.length < 10) {
    let querySnaps = await db.collection('ratings').where('in_group','==',conv.user.storage.curr_group_id).get();
    if (querySnaps.docs.length < 10) {
      return conv.followup('show_movie_event');
    }
  }

  console.log(user_ids)
  let list = await getRecs(conv,user_ids,50);
  if (list.length === 0){
    console.log('About to return followup show_movie_event');
    // conv.ask('Sorry, your personalised recommendations aren\'t ready yet - but here\'s some popular movies you haven\'t rated yet.');
    return conv.followup('show_movie_event');
  }

  conv.data.latest_recommendations = list;


  // list = list.slice(0,10);
  var moviePromises = [];
  for (let id of list){
    // if (!already_rated.includes(id)){
      moviePromises.push(db.collection('movies').doc(id).get());
    // }
  }
  let querySnaps = await Promise.all(moviePromises);

  var movieDocs = [];
  var missingMovies = [];
  for (let doc of querySnaps){
    if (doc.exists){
      if(doc.data().year[0] > 2000){
        movieDocs.push(doc.data());
      }
    } else{
      missingMovies.push(fixMissingImdb(doc.id));
    }
  }

  if (missingMovies.length > 0){
    let fixedMovies = await Promise.all(missingMovies);
    movieDocs = movieDocs.concat(fixedMovies);
  }
  movieDocs = movieDocs.slice(0,10)
  // movieDocs = movieDocs.slice(0,10);

  let list_title = `Top 10 Recommended Movies for ${conv.user.storage.curr_group_name}🍿`;
  let list_obj = movieListBuilder(conv, list_title, movieDocs);

  conv.contexts.set('movie_list_details',3,{title:list_title,items:movieDocs});
  conv.ask('Here are the top recommendations for your group! Select a movie to add it to your watchlist.');
  return defaultAsk(conv,list_obj);
});



/*
* Actions Event Handler Intents
* ---------------
* - Get Confirmation
* - Get Option
* - Help
* - Cancel
*/

// ACTIONS ON GOOGLE EVENTS

// triggered by the  `actions_intent_CONFIRMATION` event, gets a yes or no from the user
app.intent('Get Confirmation', async (conv, params, confirmation) => {
  console.log('Get Confirmation intent triggered.')

  const leaving_group  = conv.contexts.get('leaving_group');
  const confirm_rename = conv.contexts.get('confirm_rename');

  if (leaving_group){
    console.log('Context: leaving group');
    if (confirmation) {
      let previousGroup = conv.user.storage.curr_group_name;
      await leaveGroup(conv);
      conv.ask(`Okay, you have left the group ${previousGroup} 💔 Your active group is now ${conv.user.storage.curr_group_name}.`);
      return defaultAsk(conv,"What would you like to do now?");
    } else {
      conv.ask(`Okay, you will stay in the group ${conv.user.storage.curr_group_name}.`);
      return defaultAsk(conv,"What would you like to do now?");
    }
  }
  else if (confirm_rename){
    console.log('Context: confirm_rename.');
    if (confirmation) {
      conv.contexts.set('expecting_group_name',1,{});
      conv.ask(new Suggestions(SUGGS['cancel']));
      return conv.ask('Okay, what would you like to rename this group?');
    } else {
      return defaultAsk(conv,`No worries - ${conv.user.storage.curr_group_name} is a great name anyway!`);
    }
  }

  conv.ask(new Suggestions(SUGGS['default']));
  return defaultAsk(conv,'Sorry, something went wrong. Please try again.');
});

//Triggered by the `actions_intent_OPTION` event, gets a selected option from the user
app.intent('Get Option', async (conv, params, option) => {
  console.log('Get Option intent triggered.');

  var choosing_group    = conv.contexts.get('choosing_group');
  var movie_list        = conv.contexts.get('movie_list_details');
  var viewing_watchlist = conv.contexts.get('viewing_watchlist');

  if (choosing_group){
    console.log('choosing_group context matched');
    if (!option) {
      return defaultAsk(conv,'You did\'nt select an option from the list!');
    } else {
      // const new_params = {group_name: String(option)};
      // conv.contexts.set('group_chosen',1,{'group_name':String(option)})
      console.log('Option Selected: ', option);
      conv.data.chosen_group = option;
      return conv.followup('change_group_event', {});
    }
  }
  else if (movie_list) {
    console.log('movie_list context matched');
    if (!option) {
      return defaultAsk(conv,'You did\'nt select an option from the list!');
    } else {
      console.log('Option Selected: ', option);

      const chosen_movie = conv.data.list_items[option];
      console.log('Chosen movie ', chosen_movie);

      let movieCard = await buildMovieCard(conv,conv.data.list_ids[option]);
      let movie_id = conv.data.list_ids[option];
      console.log(conv.data.list_ids[option]);

      conv.contexts.set('movie_card_details',2,{title:chosen_movie.title,imdbId:movie_id}),

      // Create a basic card
      conv.ask(`Here is some more info on ${chosen_movie.title.slice(0, chosen_movie.title.length-7)}.`);
      if (viewing_watchlist){
        conv.contexts.set('viewing_watchlist',1,{movie_to_remove:movie_id,title:chosen_movie.title});
        conv.ask(new Suggestions(SUGGS['in_wlist']));
      } else {
        conv.ask(new Suggestions(SUGGS['ratings']));
      }
      return conv.ask(movieCard);
    }
  }

  console.log('No option or context matched.');
  return defaultAsk(conv,'I\'m sorry, either you didn\'t select an option from the list or something went wrong. 😵 Please try again!');
});

// Gives hints and sample invocations to help the user
app.intent('Help', async (conv, params) => {
  console.log('Help intent triggered.');

  conv.ask('I\'m here to help! I can do many things for you if you ask me for the right things. Here\'s \
  a list of things I can do.  You can also press on or speak aloud the suggestion chips at the bottom of the screen for quicker \
  interactions.');
  return defaultAsk(conv,new BasicCard({
      text:
      '**For Groups**:  \n\
      Create a new group: *"Create a new group"*  \n\
      See your groups: *"List my groups"*  \n\
      Change group: Ask to see your groups, then select a group.  \n\
      Rename a group: *"Rename this group"*  \n\
      Join a group: *"Join a group", then provide the group\'s passphrase.*  \n\
      **For Movies**:  \n\
      Get Movie Recommendations: *"What are our top recommendations?"*  \n\
      Find a movie to watch: *"Match me with a movie"*  \n\
      Add a movie to your watch list: *"Add Avatar to my watch list"*  \n\
      ',
      title: 'Movie Match Help Centre  ℹ️',
      subtitle: 'Here are some sample interactions for using Movie Match!',
  }));
});

// Cancels from any confirmation or selection stage
app.intent('Cancel', async (conv, params) => {

  console.log('Cancel intent triggered.');

  return defaultAsk(conv,'No worries, let\'s try something else.');
});
// ERRORS AND FALLBACKS
/*
* Fallbacks and Errors
*/

// Catches errors in the webhook code
app.catch((conv, error) => {
  console.error(`Error intent triggered: ${error}`);

  return defaultAsk(conv, new SimpleResponse({
    speech: `Sorry, something went wrong. Please try that again.`,
    text: `Sorry, something went wrong. 😵 Please try that again.`,
  }));
});

// Triggered when no other intents match the users input
app.fallback(async (conv,params,input) => {
  console.log('Fallback intent triggered.');
  let context = conv.contexts.get('fallback_count')

  // Keep previous contexts alive
  console.log('Reviving contexts');
  for (let context of conv.contexts){
    let full_name    = context.name.split('/');
    let trimmed_name = full_name[full_name.length-1];
    conv.contexts.set(trimmed_name,context.lifespanCount + 1,context.parameters);
  }

  if (context){
    switch (context.parameters.count){
      case 1:
        conv.contexts.set('fallback_count',1,{count:2});
        return defaultAsk(conv,"Sorry, I still didn't understand. Using the suggestion chips below might help.");
      case 2:
        conv.contexts.set('fallback_count',1,{count:3});
        return defaultAsk(conv,"I'm still not sure how to help with that. Use the suggestion chips if you're stuck.");
      case 3:
        return conv.close(new SimpleResponse({
          speech: `Sorry, something went wrong. Please try Movie Match again.`,
          text: `Sorry, something went wrong. 😵 Please try Movie Match again.`,
        }));
      default:
        conv.contexts.set('fallback_count',1,{count:1});
        return defaultAsk(conv,"I didn't quite catch that. Could you say that again?");
    }
  } else {
    conv.contexts.set('fallback_count',1,{count:1});
    return defaultAsk(conv,"I didn't quite catch that. Could you say that again?");
  }
  /* eslint-disable no-unreachable */

  conv.contexts.set('fallback_count',1,{count:1})
  return defaultAsk(conv,"I didn't quite catch that. Could you say that again?");
});

// Exports the app intents as endpoints for dialogflow
exports.fulfillment = functions.region('europe-west1').https.onRequest(app);
